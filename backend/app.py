from flask import Flask, jsonify, request, render_template
from flask_cors import CORS


# configuration
DEBUG = True

# instantiate the app
app = Flask(__name__, static_folder = "../dist/static", template_folder = "../dist")

# enable CORS
CORS(app, resources={r"/api/*": {"origins": "*"}})

@app.route('/')
def index():
    return render_template("index.html")

# sanity check route
@app.route('/ping', methods=['GET'])
def ping_pong():
    return jsonify('pong!')

if __name__ == '__main__':
    app.run()
